/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import br.com.agromap.Agromap.model.User;
import br.com.agromap.Agromap.util.RoleEnum;

/**
 *
 * @author lucasbarros
 */
public class UserBO {
    private User user;
    public UserBO() {
    }
    public UserBO(User user) {
        this.user = user;
    } 
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public User validarUsuario(User user){
        user.setRole(RoleEnum.USER.toString());
        return user;
    }
}
