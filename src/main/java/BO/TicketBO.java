package BO;

import br.com.agromap.Agromap.model.Ticket;

public class TicketBO {

	private Ticket ticket;
	
	public TicketBO(){		
	}

	public TicketBO(Ticket ticket) {
		this.ticket = ticket;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}
	
}
