package br.com.agromap.Agromap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgromapApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgromapApplication.class, args); 
	}

}
