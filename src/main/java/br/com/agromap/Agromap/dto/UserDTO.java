package br.com.agromap.Agromap.dto;


import br.com.agromap.Agromap.model.User;

public class UserDTO  extends User {

    private String email;
    private  String login;
    private  String password;

    public UserDTO(String email, String login, String password) {
        this.email = email;
        this.login = login;
        this.password = password;
    }

    public UserDTO(String email, String login, String password, String email1, String login1, String password1) {
        super(email, login, password);
        this.email = email1;
        this.login = login1;
        this.password = password1;
    }

    public UserDTO(String email, String longin, String password, String role, String email1, String login, String password1) {
        super(email, longin, password, role);
        this.email = email1;
        this.login = login;
        this.password = password1;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }
}
