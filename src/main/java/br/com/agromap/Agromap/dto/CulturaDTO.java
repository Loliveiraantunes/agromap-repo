/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.dto;

import br.com.agromap.Agromap.model.Cultura;

/**
 *
 * @author Aluno
 */
public class CulturaDTO extends Cultura {
    
    private String nome;
    private String tipo;
    private String descricao;
    private String foto;

    public CulturaDTO(String nome, String tipo, String descricao, String foto) {
        this.nome = nome;
        this.tipo = tipo;
        this.descricao = descricao;
        this.foto = foto;
    }   

    public CulturaDTO(String nome, String tipo, String descricao, String foto, String nome1, String tipol, String descricaol, String fotol) {
   
        this.nome = nome;
        this.tipo = tipol;
        this.descricao = descricaol;
        this.foto = fotol;        
               
    }

    public CulturaDTO(String nome, String tipo, String descricao, String foto, String nome1l) {
  
        this.nome = nome;
        this.tipo = tipo;
        this.descricao = descricao;
        this.foto = foto;
        
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
