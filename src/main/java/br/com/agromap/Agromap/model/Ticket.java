/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Type;

/**
 *
 * @author Aluno
 */
@Entity
@Table(name = "ticket")
public class Ticket implements Serializable {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Type(type="text")
    private String descricao;
    
    private int status;
    
    @Temporal(TemporalType.DATE)
    private Date dataCriacao;
    
    @Temporal(TemporalType.DATE)
    private Date dataFechamento;

   @JsonBackReference
    @OneToOne
    @JoinColumn(name = "id_cultivo")
    private Cultivo cultivo;
    
    @OneToOne
    @JoinColumn(name = "id_contamina")
    private Contamina contamina;

   @JsonManagedReference
    @OneToMany(mappedBy = "ticket")
    private List<Solucao> solucao;
    
    public Ticket() {
    }

    public Ticket(Long id, String descricao, int status, Date dataCriacao, Date dataFechamento) {
        this.id = id;
        this.descricao = descricao;
        this.status = status;
        this.dataCriacao = dataCriacao;
        this.dataFechamento = dataFechamento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public Contamina getContamina() {
		return contamina;
	}

	public void setContamina(Contamina contamina) {
		this.contamina = contamina;
	}

	public Cultivo getCultivo() {
		return cultivo;
	}

	public void setCultivo(Cultivo cultivo) {
		this.cultivo = cultivo;
	}

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Date getDataFechamento() {
        return dataFechamento;
    }

    public void setDataFechamento(Date dataFechamento) {
        this.dataFechamento = dataFechamento;
    }

    public List<Solucao> getSolucao() {
        return solucao;
    }

    public void setSolucao(List<Solucao> solucao) {
        this.solucao = solucao;
    }

}
