/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.model;


import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 *
 * @author Aluno
 */
@Entity
@Table(name = "cultivo")
public class Cultivo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Temporal(TemporalType.DATE)
    private Date dataInicio;
    @Temporal(TemporalType.DATE)
    private Date dataFim;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "id_propriedade")
    private Propriedade propriedade;


    @OneToOne
    @JoinColumn(name = "id_cultura")
    private Cultura cultura;
    
    @JsonManagedReference
    @OneToOne
    private Ticket ticket;
    
    
    public Cultivo() {
    }
    
    public Cultivo(Long id, Date dataInicio, Date dataFim) {
        this.id = id;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
    }
    
 
    public Propriedade getPropriedade() {
		return propriedade;
	}
    
	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    

    public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Cultura getCultura() {
        return cultura;
    }

    public void setCultura(Cultura cultura) {
        this.cultura = cultura;
    }

    @Override
    public String toString() {
        return "Cultivo{" + "id=" + id + ", dataInicio=" + dataInicio + ", dataFim=" + dataFim + ", propriedade=" + propriedade + ", cultura=" + cultura + '}';
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cultivo other = (Cultivo) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    
    
}
