/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "doenca")
public class Doenca implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Type(type="text")
    private String descricao;

    private String nome;
    private String causador;
    
    @JsonIgnore
    @OneToMany(mappedBy = "doenca")
    private List<Contamina> contamina;

    public Doenca() {
    }
    
    public Doenca(String descricao, String nome, String causador) {
        this.descricao = descricao;
        this.nome = nome;
        this.causador = causador;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


	public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCausador() {
        return causador;
    }

    public void setCausador(String causador) {
        this.causador = causador;
    }

    public List<Contamina> getContamina() {
        return contamina;
    }
    public void setContamina(List<Contamina> contamina) {
        this.contamina = contamina;
    }
}
    
   