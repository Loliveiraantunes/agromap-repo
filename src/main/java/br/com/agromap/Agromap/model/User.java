package br.com.agromap.Agromap.model;


import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "user")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String email;

    @Column(unique = true)
    private String login;

    private String password;

    private String role;

//	Passa o user como referência para as classes models, basta colocar  
//  " @JsonBackReference " Na classe em que vai receber o User. 
    
    @JsonManagedReference
    @OneToOne(mappedBy = "user")
    private Pessoa pessoa;
    
    @JsonManagedReference
    @OneToMany
    private List<Propriedade> propriedade;

    public User() {
    }

    public User(String email, String login, String password) {
        this.email = email;
        this.login = login;
        this.password = password;
    }

    public User(String email, String longin, String password, String role) {
        this.email = email;
        this.login = longin;
        this.password = password;
        this.role = role;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public List<Propriedade> getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(List<Propriedade> propriedade) {
		this.propriedade = propriedade;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
