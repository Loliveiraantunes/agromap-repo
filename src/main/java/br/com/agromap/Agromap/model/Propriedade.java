package br.com.agromap.Agromap.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "propriedade")
public class Propriedade implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String logradouro;

    @Column(nullable = false, length = 9)
    private String cep;

    @Column(nullable = false)
    private String cidade;

    @Column(nullable = false)
    private String bairro;

    @Column(nullable = false,length = 2)
    private  String uf;

    @Column(nullable = false)
    private String latitude;

    @Column(nullable = false)
    private String longitude;

    @Column(nullable = false)
    private String documento;
    
    @Column(nullable = false)
    private String kmQuadrados;
    
    @Column(nullable = false)
    private String numero;
    
    @Temporal(TemporalType.DATE)
    private Date cadastro;


    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;
    
    @JsonManagedReference
    @OneToMany(mappedBy = "propriedade")
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<Cultivo> cultivo;

    public Propriedade() {
    }

    public Propriedade(String logradouro, String cep, String cidade, String bairro, String uf, String latitude, String longitude, String documento, String kmQuadrados, String numero, Date cadastro) {
        this.logradouro = logradouro;
        this.cep = cep;
        this.cidade = cidade;
        this.bairro = bairro;
        this.uf = uf;
        this.latitude = latitude;
        this.longitude = longitude;
        this.documento = documento;
        this.kmQuadrados = kmQuadrados;
        this.numero = numero;
        this.cadastro = cadastro;
    }

	public List<Cultivo> getCultivo() {
		return cultivo;
	}

	public void setCultivo(List<Cultivo> cultivo) {
		this.cultivo = cultivo;
	}

	public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getKmQuadrados() {
		return kmQuadrados;
	}

	public void setKmQuadrados(String kmQuadrados) {
		this.kmQuadrados = kmQuadrados;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getCadastro() {
        return cadastro;
    }

    public void setCadastro(Date cadastro) {
        this.cadastro = cadastro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Propriedade that = (Propriedade) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(logradouro, that.logradouro) &&
                Objects.equals(cep, that.cep) &&
                Objects.equals(cidade, that.cidade) &&
                Objects.equals(bairro, that.bairro) &&
                Objects.equals(uf, that.uf) &&
                Objects.equals(latitude, that.latitude) &&
                Objects.equals(longitude, that.longitude) &&
                Objects.equals(documento, that.documento) &&
                Objects.equals(cadastro, that.cadastro) &&
                Objects.equals(kmQuadrados, that.kmQuadrados) &&
        		Objects.equals(numero, that.numero);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, logradouro, cep, cidade, bairro, uf, latitude, longitude, documento, cadastro, kmQuadrados, numero);
    }
}
