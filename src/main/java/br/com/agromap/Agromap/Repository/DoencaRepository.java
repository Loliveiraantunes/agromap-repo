/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.agromap.Agromap.model.Doenca;
import br.com.agromap.Agromap.model.User;

/**
 *
 * @author Aluno
 */
public interface DoencaRepository extends CrudRepository<Doenca, Long> {
    
    
}
