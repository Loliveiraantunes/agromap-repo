/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.Repository;

import java.util.List;
import java.util.Optional;

import br.com.agromap.Agromap.model.Solucao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.agromap.Agromap.model.Ticket;
import org.springframework.stereotype.Repository;


public interface TicketRepository extends CrudRepository<Ticket, Long> {
	
	@Query("SELECT t FROM Ticket t WHERE t.id = ?1")
	Ticket findTicketById(Long id);

	@Query("SELECT t FROM Ticket t WHERE t.cultivo.id = ?1")
	Optional<Ticket> findTicketByCultivo(Long id);

	@Query("SELECT s FROM Solucao s WHERE s.ticket.id = ?1")
	List<Solucao> findTicketBySolucao(Long id_ticket);

	@Query("SELECT t FROM Ticket t ORDER BY t.status ASC")
	Iterable<Ticket> findAll();
	
}
