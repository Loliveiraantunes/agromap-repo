package br.com.agromap.Agromap.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.agromap.Agromap.model.Propriedade;
import br.com.agromap.Agromap.model.User;


public interface PropriedadeRepository extends CrudRepository<Propriedade, Long> {

	@Query("SELECT p FROM Propriedade p WHERE p.user.id = ?1")
	List<Propriedade> findByUserId(Long id);

	@Query("SELECT p FROM Propriedade p INNER JOIN Cultivo c ON c.propriedade.id = p.id INNER JOIN Ticket t ON t.cultivo.id = c.id WHERE t.status = 3 GROUP BY p.id")
	List<Propriedade> findMarkers();

}
