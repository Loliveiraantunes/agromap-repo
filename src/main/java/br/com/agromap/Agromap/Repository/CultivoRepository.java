/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.Repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.agromap.Agromap.model.Cultivo;

/**
 *
 * @author Aluno
 */
public interface CultivoRepository extends CrudRepository<Cultivo, Long> {
    
	@Query("SELECT c FROM Cultivo c WHERE c.propriedade.id = ?1")
	List<Cultivo> findCultByProp(Long id_propriedade);
	
}
