package br.com.agromap.Agromap.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.agromap.Agromap.model.Solucao;
import br.com.agromap.Agromap.model.Ticket;

public interface SolucaoRepository extends CrudRepository<Solucao, Long>{

	
}
