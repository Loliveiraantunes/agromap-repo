/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.agromap.Agromap.model.Tratamento;

/**
 *
 * @author Aluno
 */
public interface TratamentoRepository extends CrudRepository<Tratamento, Long> {

	/*@Query("SELECT t FROM Tratamento t WHERE t.solucao.id = ?1")
	List<Tratamento> findTratamentoBySolucao(Long id_solucao);*/
    
}
