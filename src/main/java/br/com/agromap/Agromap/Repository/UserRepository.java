package br.com.agromap.Agromap.Repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.agromap.Agromap.model.Ticket;
import br.com.agromap.Agromap.model.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE (u.login = ?1 or u.email = ?3 ) and u.password = ?2")
    List<User> getLogin(String login, String senha, String email);

    @Query("SELECT t FROM Ticket t INNER JOIN Cultivo c ON c.ticket.id = t.id INNER JOIN Propriedade p ON c.propriedade.id = p.id INNER JOIN User u ON p.user.id = u.id WHERE u.id = ?1")  
    		List<Ticket> getUserTicket(Long id);
}
 
 