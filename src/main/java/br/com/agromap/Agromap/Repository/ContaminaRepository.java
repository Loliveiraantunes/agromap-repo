/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.agromap.Agromap.model.Contamina;

/**
 *
 * @author Aluno
 */
public interface ContaminaRepository extends CrudRepository<Contamina, Long> {
    
	@Query("SELECT c FROM Contamina c WHERE c.cultura.id = ?1")
	List<Contamina> findByCulturaId(Long id_cultura);
	
}
