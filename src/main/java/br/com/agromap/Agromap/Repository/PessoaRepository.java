package br.com.agromap.Agromap.Repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.agromap.Agromap.model.Pessoa;
import br.com.agromap.Agromap.model.Ticket;


public interface PessoaRepository extends CrudRepository<Pessoa, Long>{
	
	@Query("SELECT p FROM Pessoa p WHERE p.user.id = ?1")
	Optional<Pessoa> findPessoaByUserId(Long id);
	
	@Query("SELECT p FROM Pessoa p WHERE p.cpf = ?1")
	List<Pessoa> findCPFByPessoaId(String cpf);
}
