package br.com.agromap.Agromap.Controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.agromap.Agromap.Repository.SolucaoRepository;
import br.com.agromap.Agromap.model.Solucao;

	@Controller
@RequestMapping(path = "api/solucao")
public class SolucaoController {
		
		
	private final SolucaoRepository solucaoRepository;

	@Autowired
	public SolucaoController(SolucaoRepository solucaoRepository) {
		this.solucaoRepository = solucaoRepository;
	}
	
	public ResponseEntity<Void> removeSolucao(@PathVariable Long id){
		try{
			solucaoRepository.deleteById(id);
		}catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
        return new ResponseEntity<>(HttpStatus.OK);
	}

		@DeleteMapping(value = "/remove/{id}")
		public ResponseEntity<Void> removeTicket(@PathVariable Long id){
			try{
				solucaoRepository.deleteById(id);
			}catch (Exception e){
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<>(HttpStatus.OK);
		}
	
	@PutMapping(path = "/update/{id}")
	public Optional<Solucao> updateSolucao(@RequestBody Solucao solucao, @PathVariable Long id){
		Optional<Solucao> s = solucaoRepository.findById(id);
		return s;
	}

		@GetMapping(path="/all")
		public @ResponseBody Iterable<Solucao> getAllTickets() {
			return solucaoRepository.findAll();
		}


		@PostMapping(path = "/add")
	public ResponseEntity<Solucao> addSolucao(@RequestBody Solucao solucao){
		Solucao s = solucaoRepository.save(solucao);
		return new ResponseEntity<> (s, HttpStatus.OK);
	}
	
	@GetMapping(path = "/{id}")
	public @ResponseBody
	Optional<Solucao> getById(@PathVariable Long id){
		return solucaoRepository.findById(id);
	}
	
}
