/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.Controller;
import br.com.agromap.Agromap.Repository.ContaminaRepository;
import br.com.agromap.Agromap.Repository.CultivoRepository;
import br.com.agromap.Agromap.Repository.CulturaRepository;
import br.com.agromap.Agromap.model.Contamina;
import br.com.agromap.Agromap.model.Cultura;

import java.util.Optional;

import javax.swing.text.DefaultEditorKit.CutAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 *
 * @author Aluno
 */
@Controller
@RequestMapping(path = "api/cultura")
public class CulturaController {
    
    private final CulturaRepository culturaRepository;
    private final ContaminaRepository contaminaRepository;

    @Autowired
    public CulturaController(CulturaRepository culturaRepository, ContaminaRepository contaminaRepository) {
        this.culturaRepository = culturaRepository;
        this.contaminaRepository = contaminaRepository;
    }
 

    @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity<Void> removeCultura(@PathVariable Long id){
         try{
        	 Optional<Cultura> cl = culturaRepository.findById(id);
        	 cl.ifPresent(cultura -> {
        		 for(Contamina contamina : cultura.getContamina() ) {
        			 contaminaRepository.delete(contamina);
        		 }
        	 });
             culturaRepository.deleteById(id);
         }catch (Exception e){
             return new ResponseEntity<>(HttpStatus.NOT_FOUND);
         }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Cultura> updateCultura(@RequestBody Cultura cultura){
        Cultura c = culturaRepository.save(cultura);
        return new ResponseEntity<>(c, HttpStatus.OK);
    }

    @PostMapping(value = "/add")
    public ResponseEntity<Cultura> addCultura(@RequestBody Cultura cultura){
        Cultura c = culturaRepository.save(cultura);
        return new ResponseEntity<>(c, HttpStatus.OK);
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Cultura> getAllCulturas() {
        return culturaRepository.findAll();
    }
     
    @GetMapping(path = "/{id}")
    public @ResponseBody
    Optional<Cultura> getById(@PathVariable Long id){
    	return culturaRepository.findById(id);
    }
}
