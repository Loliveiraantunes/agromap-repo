/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.Controller;

import br.com.agromap.Agromap.Repository.CultivoRepository;
import br.com.agromap.Agromap.Repository.TicketRepository;
import br.com.agromap.Agromap.model.Cultivo;
import br.com.agromap.Agromap.model.Ticket;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Aluno
 */
@Controller
@RequestMapping(path = "api/cultivo")
public class CultivoController {
    
    private final CultivoRepository cultivoRepository;
    private final TicketRepository ticketRepository;

    @Autowired
    public CultivoController(CultivoRepository cultivoRepository, TicketRepository ticketRepository) {
        this.cultivoRepository = cultivoRepository;
        this.ticketRepository = ticketRepository;
    }
 

    @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity<Void> removeCultivo(@PathVariable Long id){
         try{
             Optional<Cultivo> ct = cultivoRepository.findById(id);
             ct.ifPresent(cult ->{
                 ticketRepository.deleteById(cult.getTicket().getId());
             });
             cultivoRepository.deleteById(id);
         }catch (Exception e){
             return new ResponseEntity<>(HttpStatus.NOT_FOUND);
         }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Cultivo> updateCultivo(@RequestBody Cultivo cultivo){
        Optional<Cultivo> ct = cultivoRepository.findById(cultivo.getId());
        ct.ifPresent( cult ->{
            cultivo.setPropriedade(cult.getPropriedade());
        });

        Cultivo c = cultivoRepository.save(cultivo);
        return new ResponseEntity<>(c, HttpStatus.OK);
    }

    @PostMapping(value = "/add")
    public ResponseEntity<Cultivo> addCultivo(@RequestBody Cultivo cultivo){
        Cultivo c = cultivoRepository.save(cultivo);
        return new ResponseEntity<>(c, HttpStatus.CREATED);
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Cultivo> getAllCultivos() {
        return cultivoRepository.findAll();
    }
    
    @GetMapping(path = "/{id}")
    public @ResponseBody
    Optional<Cultivo> getById(@PathVariable Long id){
    	return cultivoRepository.findById(id);
    }
    
    @GetMapping(path = "/propriedade/{id_propriedade}")
    public @ResponseBody
    List<Cultivo> findCultivoByPropriedade(@PathVariable Long id_propriedade){
    	List<Cultivo> c = cultivoRepository.findCultByProp(id_propriedade);
    	return c;
    }
    
}
