package br.com.agromap.Agromap.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.agromap.Agromap.Repository.PessoaRepository;
import br.com.agromap.Agromap.Repository.UserRepository;
import br.com.agromap.Agromap.model.Pessoa;



@Controller
@RequestMapping(path = "api/pessoa")
public class PessoaController {
	
	private final PessoaRepository pessoaRepository;
	
	@Autowired
	public PessoaController(PessoaRepository pessoaRepository) {
		this.pessoaRepository = pessoaRepository;
	}
	

	@DeleteMapping(value = "/remove/{id}")
	public ResponseEntity<Void> removePessoa(@PathVariable Long id){
		
		try {
			pessoaRepository.deleteById(id);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
			return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PutMapping(value = "/update/{id}")
	public ResponseEntity<Pessoa> updatePessoa(@RequestBody Pessoa pessoa){
        Pessoa p = pessoaRepository.save(pessoa);
        return new ResponseEntity<>(p, HttpStatus.OK);
    }

	
	@PostMapping(value = "/add")
	public ResponseEntity<Pessoa> addPessoa(@RequestBody Pessoa pessoa){
		Pessoa p = pessoaRepository.save(pessoa);
		return new ResponseEntity<> (p, HttpStatus.OK);
	}
		
	@GetMapping(path="/{id}")
	public @ResponseBody 
	Optional<Pessoa> findOneId(@PathVariable Long id){
		Optional<Pessoa> pe = pessoaRepository.findById(id);
		return pe;
	}
	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<Pessoa> getallPessoa(){
		return pessoaRepository.findAll();
	}
	
	@GetMapping(path = "user/{id_user}")
	public @ResponseBody
	Optional<Pessoa> findPByU(@PathVariable Long id_user){
		return pessoaRepository.findPessoaByUserId(id_user);
	}
	
	@GetMapping(path = "pessoa/cpf/{cpf}")
	public @ResponseBody
	List<Pessoa> findCPFByPessoa(@PathVariable String cpf){
		List<Pessoa> p = pessoaRepository.findCPFByPessoaId(cpf);
		return p;
	}
}
