/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.Controller;

import br.com.agromap.Agromap.Repository.TicketRepository;
import br.com.agromap.Agromap.Repository.UserRepository;
import br.com.agromap.Agromap.model.Solucao;
import br.com.agromap.Agromap.model.Ticket;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "api/ticket")
public class TicketController {

    private final TicketRepository ticketRepository;
    private final UserRepository userRepository;

    @Autowired
    public TicketController(TicketRepository ticketRepository, UserRepository userRepository) {
        this.ticketRepository = ticketRepository;
        this.userRepository = userRepository;
    }
 

    @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity<Void> removeTicket(@PathVariable Long id){
         try{
             ticketRepository.deleteById(id);
         }catch (Exception e){
             return new ResponseEntity<>(HttpStatus.NOT_FOUND);
         }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Ticket> updateTicket(@RequestBody Ticket ticket){
    	Ticket t = ticketRepository.save(ticket);
        return new ResponseEntity<>(t,HttpStatus.OK);
    }

    @PostMapping(value = "/add")
    public ResponseEntity<Ticket> addTicket(@RequestBody Ticket ticket){
        Ticket t = ticketRepository.save(ticket);
        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Ticket> getAllTickets() {
        return ticketRepository.findAll();
    }
    
    @GetMapping(path = "/{id}")
    public @ResponseBody
    Optional<Ticket> getById(@PathVariable Long id){
    	return ticketRepository.findById(id);
    }


    @GetMapping(path = "/cultivo/{id_cultivo}")
    public @ResponseBody Optional<Ticket> getByCutivo(@PathVariable Long id_cultivo ){
        return  ticketRepository.findTicketByCultivo(id_cultivo);
    }

    @GetMapping(path = "/ticketAberto/{id_ticket}")
    public @ResponseBody Ticket getTicketAberto(@PathVariable Long id_ticket){
    	Ticket t = ticketRepository.findTicketById(id_ticket);
    	return t;
    }
    
    @GetMapping(path = "/solucao/{id_ticket}")
	public @ResponseBody
    List<Solucao> getTicketById(@PathVariable Long id_ticket){
        List<Solucao> t = ticketRepository.findTicketBySolucao(id_ticket);
		return t;
	}   
 }