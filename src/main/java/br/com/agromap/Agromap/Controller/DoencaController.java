/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.Controller;

import br.com.agromap.Agromap.Repository.DoencaRepository;
import br.com.agromap.Agromap.Repository.PropriedadeRepository;
import br.com.agromap.Agromap.model.Cultura;
import br.com.agromap.Agromap.model.Doenca;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;

@Controller
@RequestMapping(path = "api/doenca")
public class DoencaController {
   

    private final DoencaRepository doencaRepository;

    @Autowired
    public DoencaController(DoencaRepository doencaRepository) {
        this.doencaRepository = doencaRepository;
    }
     @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity<Void> removeDoenca(@PathVariable Long id){
         try{
             doencaRepository.deleteById(id);
         }catch (Exception e){
             return new ResponseEntity<>(HttpStatus.NOT_FOUND);
         }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Doenca> updateDoenca(@RequestBody Doenca doenca){
        Doenca d = doencaRepository.save(doenca);
        return new ResponseEntity<>(d, HttpStatus.OK);
    }

    @PostMapping(value = "/add")
    public ResponseEntity<Doenca> addDoenca(@RequestBody Doenca doenca){
        Doenca d = doencaRepository.save(doenca);
        return new ResponseEntity<>(d, HttpStatus.OK);
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Doenca> getAllCulturas() {
        return doencaRepository.findAll();
    }

    @GetMapping(path="/{id}")
    public @ResponseBody
    Optional<Doenca> getById(@PathVariable Long id) {
        return doencaRepository.findById(id);
    }
}
