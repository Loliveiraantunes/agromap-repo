/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.Controller;

import br.com.agromap.Agromap.Repository.TratamentoRepository;
import br.com.agromap.Agromap.model.Ticket;
import br.com.agromap.Agromap.model.Tratamento;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Aluno
 */
 
    @Controller
@RequestMapping(path = "api/tratamento")
public class TratamentoController {

    private final TratamentoRepository tratamentoRepository;

    @Autowired
    public TratamentoController(TratamentoRepository tratamentoRepository) {
        this.tratamentoRepository = tratamentoRepository;
    }
 

    @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity<Void> removeTratamento(@PathVariable Long id){
         try{
             tratamentoRepository.deleteById(id);
         }catch (Exception e){
             return new ResponseEntity<>(HttpStatus.NOT_FOUND);
         }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Tratamento> updateTratamento(@RequestBody Tratamento tratamento){
        Tratamento t = tratamentoRepository.save(tratamento);
        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @PostMapping(value = "/add")
    public ResponseEntity<Tratamento> addTratamento(@RequestBody Tratamento tratamento){
        Tratamento t = tratamentoRepository.save(tratamento);
        return new ResponseEntity<>(t, HttpStatus.CREATED);
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Tratamento> getAllTratamentos() {
        return tratamentoRepository.findAll();
    }
    
    @GetMapping(path = "/{id}")
    public @ResponseBody
    Optional<Tratamento> getById(@PathVariable Long id){
    	return tratamentoRepository.findById(id);
    }
  
   /* @GetMapping(path = "/solucao/{id_solucao}")
	public @ResponseBody List<Tratamento> getTratamentoById(@PathVariable Long id_solucao){
		List<Tratamento> t = tratamentoRepository.findTratamentoBySolucao(id_solucao);
		return t;
    }*/
}
