/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agromap.Agromap.Controller;

import br.com.agromap.Agromap.Repository.ContaminaRepository;
import br.com.agromap.Agromap.Repository.TicketRepository;
import br.com.agromap.Agromap.model.Contamina;
import br.com.agromap.Agromap.model.User;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Aluno
 */
    
    @Controller
@RequestMapping(path = "api/contamina")
public class ContaminaController {

    private final ContaminaRepository contaminaRepository;
    private final TicketRepository ticketRepository;

    @Autowired
    public ContaminaController(ContaminaRepository contaminaRepository, TicketRepository ticketRepository) {
        this.contaminaRepository = contaminaRepository;
        this.ticketRepository = ticketRepository;
    }
 

    @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity<Void> removeContamina(@PathVariable Long id){
         try{
             contaminaRepository.deleteById(id);
             ticketRepository.deleteById(id);
         }catch (Exception e){
             return new ResponseEntity<>(HttpStatus.NOT_FOUND);
         }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Contamina> updateContamina(@RequestBody Contamina contamina){
        Contamina c = contaminaRepository.save(contamina);
        return new ResponseEntity<>(c, HttpStatus.OK);
    }

    @PostMapping(value = "/add")
    public ResponseEntity<Contamina> addContamina(@RequestBody Contamina contamina){
        Contamina c = contaminaRepository.save(contamina);
        return new ResponseEntity<>(c, HttpStatus.OK);
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Contamina> getAllContaminas() {
        return contaminaRepository.findAll();
    }  
    
    @GetMapping(path = "/{id}")
    public @ResponseBody
    Optional<Contamina> getById(@PathVariable Long id){
    	return contaminaRepository.findById(id);
    }
    
    @GetMapping(path = "/cultura/{id_cultura}")
    public @ResponseBody 
    List<Contamina> findIdCultura(@PathVariable Long id_cultura){
    	List<Contamina> c = contaminaRepository.findByCulturaId(id_cultura);
    	return c;
    }
}
