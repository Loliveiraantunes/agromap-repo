package br.com.agromap.Agromap.Controller;


import br.com.agromap.Agromap.Repository.CultivoRepository;
import br.com.agromap.Agromap.Repository.PropriedadeRepository;
import br.com.agromap.Agromap.Repository.UserRepository;
import br.com.agromap.Agromap.model.Cultivo;
import br.com.agromap.Agromap.model.Pessoa;
import br.com.agromap.Agromap.model.Propriedade;
import br.com.agromap.Agromap.model.User;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(path = "api/propriedade")
public class PropriedadeController {
	
    private final PropriedadeRepository propriedadeRepository;
    private final CultivoRepository cultivoRepository;
    
    @Autowired
    public PropriedadeController(PropriedadeRepository propriedadeRepository, CultivoRepository cultivoRepository) {
        this.propriedadeRepository = propriedadeRepository;
        this.cultivoRepository = cultivoRepository;
    }      

    @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity<Void> removePropriedade(@PathVariable Long id){
    	try {
         Optional<Propriedade> pl = propriedadeRepository.findById(id);
            pl.ifPresent( propriedade -> {
                for (Cultivo cultivo: propriedade.getCultivo() ) {
                    cultivoRepository.delete(cultivo);
                }
            });
            propriedadeRepository.deleteById(id);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    		return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PutMapping(value = "/update/{id}")
    public ResponseEntity<Propriedade> updatePropriedade(@RequestBody Propriedade propriedade) {
    	Propriedade pr = propriedadeRepository.save(propriedade);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }
    
    @PostMapping(value = "/add")
    public ResponseEntity<Propriedade> addPropriedade(@RequestBody Propriedade propriedade){
    	Propriedade pd = propriedadeRepository.save(propriedade);
    	return new ResponseEntity<> (pd, HttpStatus.OK);
    	
    }
    
    @GetMapping(value = "/all")
    public @ResponseBody Iterable<Propriedade> getAllPropriedade(){
        return  propriedadeRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    public @ResponseBody
    Optional<Propriedade> findOneId(@PathVariable Long id){
    	Optional<Propriedade> pl = propriedadeRepository.findById(id);
    	return pl;
    } 
    
    @GetMapping(value = "/user/{id_user}" )
    public @ResponseBody 
    List<Propriedade> findUserId(@PathVariable Long id_user){
    	List<Propriedade> p = propriedadeRepository.findByUserId(id_user);
    	return p;
    }

    @GetMapping(value = "/markers")
    public @ResponseBody List<Propriedade> getMarkers(){
        return propriedadeRepository.findMarkers();
    }
}
