package br.com.agromap.Agromap.Controller;

import BO.UserBO;
import br.com.agromap.Agromap.Repository.UserRepository;
import br.com.agromap.Agromap.model.Ticket;
import br.com.agromap.Agromap.model.User;
import br.com.agromap.Agromap.util.RoleEnum;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "api/user")
public class UserController {

    private final UserRepository userRepository;
    private UserBO userBO;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
        userBO = new UserBO();
    }

    @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity<Void> removeUser(@PathVariable Long id) {
        try {
            userRepository.deleteById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        user = userBO.validarUsuario(user);
        User u = userRepository.save(user);
        return new ResponseEntity<>(u, HttpStatus.OK);
    }

    @PostMapping(value = "/add")
    public ResponseEntity<User> addUser(@RequestBody User user) {
        user = userBO.validarUsuario(user);
        User u = userRepository.save(user);
        return new ResponseEntity<>(u, HttpStatus.CREATED);
    }

    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    @PostMapping(path = "/login")
    public @ResponseBody
    ResponseEntity<User> getLogin(@RequestBody User user) {
        for (User u : userRepository.getLogin(user.getLogin(), user.getPassword(), user.getEmail())) {
            return new ResponseEntity<>(u, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/{id}")
    public @ResponseBody
    Optional<User> getById(@PathVariable Long id){
    	return userRepository.findById(id);
    }
    
    @GetMapping(path = "/UserTicket/{id}")
    public @ResponseBody List<Ticket> getAllTicketsByUser(@PathVariable Long id){
    	return userRepository.getUserTicket(id); 
    }
}
